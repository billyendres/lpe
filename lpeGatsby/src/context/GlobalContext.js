import React, { useState, createContext } from "react";

export const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
  const [postCode, setPostCode] = useState(4560);
  const [adultsNoBill, setAdultsNoBill] = useState("");
  const [childrenNoBill, setChildrenNoBill] = useState("");
  const [adults, setAdults] = useState("");
  const [child, setChild] = useState(0);
  const [billData, setBillData] = useState([
    {
      name: "Your Bill",
      value: 0,
      color: "#37B4E4",
    },
    {
      name: "Locationâ€™s Average Bill",
      value: 527,
      color: "#A2DFF7",
    },
    {
      name: "Bills of similarly sized homes",
      value: 0,
      color: "#68819D",
    },
  ]);

  return (
    <GlobalContext.Provider
      value={[
        postCode,
        setPostCode,
        billData,
        setBillData,
        adultsNoBill,
        setAdultsNoBill,
        childrenNoBill,
        setChildrenNoBill,
        adults,
        setAdults,
        child,
        setChild,
      ]}
    >
      {children}
    </GlobalContext.Provider>
  );
};
