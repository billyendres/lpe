import React, { useState, useRef, useEffect, useContext } from "react";
import { Link } from "gatsby";
import styled from "styled-components";
import backgroundImg from "./images/background2.jpg";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import { GlobalContext } from "../context/GlobalContext";
import GoogleMaps from "../components/GoogleMaps";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: "0.25rem 0",
    minWidth: 150,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: "5%",
    height: "18rem",
    width: "22rem",
    boxShadow: theme.shadows[5],
    padding: "1.5rem",
    outline: "none",
    border: "none",
  },
}));

const ProfileBuilder = () => {
  const [labelWidth, setLabelWidth] = useState(0);
  const [checked, setChecked] = useState(false);
  const [home, setHome] = useState("");
  const [bedrooms, setBedrooms] = useState("");
  const [bathrooms, setBathrooms] = useState("");

  const [
    postCode,
    setPostCode,
    billData,
    setBillData,
    adultsNoBill,
    setAdultsNoBill,
    childrenNoBill,
    setChildrenNoBill,
    adults,
    setAdults,
    child,
    setChild,
  ] = useContext(GlobalContext);

  const classes = useStyles();

  const inputLabel = useRef(null);
  useEffect(() => {
    setLabelWidth(100);
  }, []);

  const averageBill = () => {
    if (child + adults === 1) {
      billData[2].value = 356;
      setBillData(billData);
    } else if (child + adults === 2) {
      billData[2].value = 517;
      setBillData(billData);
    } else if (child + adults === 3) {
      billData[2].value = 584;
      setBillData(billData);
    } else if (child + adults === 4) {
      billData[2].value = 654;
      setBillData(billData);
    } else if (child + adults > 4) {
      billData[2].value = 711;
      setBillData(billData);
    }
  };

  return (
    <>
      <Background src={backgroundImg} alt={backgroundImg} />
      <Wrap>
        <TextWrap>
          <SubHeader>Energy Tool</SubHeader>
          <Header>Create your energy profile</Header>
        </TextWrap>
        <BoxWrap>
          <BoxSmall>
            <BoxTitle>1. Your Typical Bill</BoxTitle>
            <BoxContent>How much was your bill last quarter?</BoxContent>
            <form className={classes.formControl} noValidate autoComplete="off">
              <TextField
                type="number"
                onChange={e => {
                  billData[0].value = e.target.value;
                  setBillData(billData);
                }}
                label="$ _ _ _ _"
                variant="outlined"
              />
            </form>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                marginTop: "1rem",
              }}
            >
              <div
                style={{
                  width: "9rem",
                  display: "inline-block",
                  marginTop: "0.5rem",
                }}
              >
                <BoxTitle>No Previous Bill?</BoxTitle>
              </div>
              <>
                <Checkbox
                  onClick={e => setChecked(e.target.checked)}
                  checked={checked}
                  value="primary"
                  color="primary"
                  inputProps={{ "aria-label": "primary checkbox" }}
                />
                <Modal
                  aria-labelledby="transition-modal-title"
                  aria-describedby="transition-modal-description"
                  className={classes.modal}
                  open={checked}
                  onClose={e => setChecked(e.target.checked)}
                  closeAfterTransition
                  BackdropComponent={Backdrop}
                  BackdropProps={{
                    timeout: 500,
                  }}
                >
                  <Fade in={checked}>
                    <div className={classes.paper}>
                      <BoxTitle>4. Who Lives With You</BoxTitle>
                      <BoxContent style={{ marginBottom: "1.5rem" }}>
                        How many people live in your home?
                      </BoxContent>
                      <FormControl
                        variant="outlined"
                        className={classes.formControl}
                        style={{ marginRight: "2rem" }}
                      >
                        <InputLabel
                          ref={inputLabel}
                          id="demo-simple-select-outlined-label"
                        >
                          Adults
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-outlined-label"
                          id="demo-simple-select-outlined"
                          value={adultsNoBill}
                          onChange={e => setAdultsNoBill(e.target.value)}
                          labelWidth={labelWidth}
                        >
                          <MenuItem value={1}>1</MenuItem>
                          <MenuItem value={2}>2</MenuItem>
                          <MenuItem value={3}>3</MenuItem>
                          <MenuItem value={4}>4+</MenuItem>
                        </Select>
                      </FormControl>
                      <FormControl
                        variant="outlined"
                        className={classes.formControl}
                      >
                        <InputLabel
                          ref={inputLabel}
                          id="demo-simple-select-outlined-label"
                        >
                          Children
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-outlined-label"
                          id="demo-simple-select-outlined"
                          value={childrenNoBill}
                          onChange={e => setChildrenNoBill(e.target.value)}
                          labelWidth={labelWidth}
                        >
                          <MenuItem value={0}>0</MenuItem>
                          <MenuItem value={1}>1</MenuItem>
                          <MenuItem value={2}>2</MenuItem>
                          <MenuItem value={3}>3</MenuItem>
                          <MenuItem value={4}>4+</MenuItem>
                        </Select>
                      </FormControl>
                      <BoxContent style={{ marginTop: "1.5rem" }}>
                        See The Average Bill For A Home Your Size.
                      </BoxContent>
                      <Link to="/average">
                        <Button
                          onClick={averageBill()}
                          style={{ marginTop: "1rem" }}
                        >
                          Learn More
                        </Button>
                      </Link>
                    </div>
                  </Fade>
                </Modal>
              </>
            </div>
          </BoxSmall>
          <BoxSmall>
            <BoxTitle>2. Your Home</BoxTitle>
            <BoxContent>What type of home do you live in?</BoxContent>
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel
                ref={inputLabel}
                id="demo-simple-select-outlined-label"
              >
                Please Select
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={home}
                onChange={e => setHome(e.target.value)}
                labelWidth={labelWidth}
              >
                <MenuItem value={"House"}>House</MenuItem>
                <MenuItem value={"Apartment"}>Apartment</MenuItem>
                <MenuItem value={"Studio"}>Studio</MenuItem>
              </Select>
            </FormControl>
          </BoxSmall>
          <BoxSmall>
            <BoxTitle>3. Your Home Size</BoxTitle>
            <BoxContent>How many rooms does your home have?</BoxContent>
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel
                ref={inputLabel}
                id="demo-simple-select-outlined-label"
              >
                Bedrooms
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={bedrooms}
                onChange={e => setBedrooms(e.target.value)}
                labelWidth={labelWidth}
              >
                <MenuItem value={1}>1</MenuItem>
                <MenuItem value={2}>2</MenuItem>
                <MenuItem value={3}>3</MenuItem>
                <MenuItem value={4}>4+</MenuItem>
              </Select>
            </FormControl>
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel
                ref={inputLabel}
                id="demo-simple-select-outlined-label"
              >
                Bathrooms
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={bathrooms}
                onChange={e => setBathrooms(e.target.value)}
                labelWidth={labelWidth}
              >
                <MenuItem value={1}>1</MenuItem>
                <MenuItem value={2}>2</MenuItem>
                <MenuItem value={3}>3+</MenuItem>
              </Select>
            </FormControl>
          </BoxSmall>
          <BoxSmall>
            <BoxTitleLarge>Bill Breakdown</BoxTitleLarge>
            <BoxContentLarge>
              Find out what makes up your bills, and how it compares to the
              households around you.
            </BoxContentLarge>
            {home && bedrooms && adults && bathrooms && billData[0].value ? (
              <Link to="/breakdown">
                <Button>Learn More</Button>
              </Link>
            ) : (
              <Button>Enter Required Fields</Button>
            )}
          </BoxSmall>
          <BoxSmall>
            <BoxTitle>4. Who Lives With You</BoxTitle>
            <BoxContent>How many people live in your home?</BoxContent>
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel
                ref={inputLabel}
                id="demo-simple-select-outlined-label"
              >
                Adults
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={adults}
                onChange={e => setAdults(e.target.value)}
                labelWidth={labelWidth}
              >
                <MenuItem value={1}>1</MenuItem>
                <MenuItem value={2}>2</MenuItem>
                <MenuItem value={3}>3</MenuItem>
                <MenuItem value={4}>4+</MenuItem>
              </Select>
            </FormControl>
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel
                ref={inputLabel}
                id="demo-simple-select-outlined-label"
              >
                Children
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={child}
                onChange={e => setChild(e.target.value)}
                labelWidth={labelWidth}
              >
                <MenuItem value={0}>0</MenuItem>
                <MenuItem value={1}>1</MenuItem>
                <MenuItem value={2}>2</MenuItem>
                <MenuItem value={3}>3</MenuItem>
                <MenuItem value={4}>4+</MenuItem>
              </Select>
            </FormControl>
          </BoxSmall>
          <BoxLarge>
            <BoxTitle style={{ display: "inline-block" }}>5. Location</BoxTitle>
            <BoxContent style={{ display: "inline-block", marginLeft: "3rem" }}>
              Where is your home located?
            </BoxContent>
            {/* <form className={classes.formControl} noValidate autoComplete="off">
              <TextField
                onChange={e => setPostCode(e.target.value)}
                label="Post Code"
                variant="outlined"
              />
            </form> */}
            <GoogleMaps />
          </BoxLarge>
          <BoxSmall>
            <BoxTitleLarge>Solar Calculator</BoxTitleLarge>
            <BoxContentLarge>
              Find out how much it costs to install solar & battery, tailored to
              your needs.
            </BoxContentLarge>
            {home && bedrooms && adults && bathrooms && billData[0].value ? (
              <Link to="/solar">
                <Button>Learn More</Button>
              </Link>
            ) : (
              <Button>Enter Required Fields</Button>
            )}
          </BoxSmall>
        </BoxWrap>
      </Wrap>
    </>
  );
};

export default ProfileBuilder;

const Background = styled.img`
  position: fixed;
  width: 100vw;
  height: 100vh;
  object-fit: cover;
  margin: 0;
`;

const Wrap = styled.div`
  position: absolute;
  z-index: 1;
  top: 10%;
  left: 5%;
  right: 5%;
  bottom: 5%;
`;

const TextWrap = styled.div`
  z-index: 1;
  text-align: center;
  margin: 2rem 0.5rem;
`;

const Header = styled.h1`
  font-style: normal;
  font-weight: bold;
  font-size: 2.5rem;
  line-height: 110%;
  color: #ffff;
  margin: 0;
  text-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  @media (max-width: 860px) {
    font-size: 1.75rem;
  }
`;

const SubHeader = styled.h2`
  font-style: normal;
  font-weight: 600;
  font-size: 1.25rem;
  line-height: 150%;
  text-transform: uppercase;
  color: #ffff;
  margin: 0;
  text-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  @media (max-width: 860px) {
    font-size: 1rem;
  }
`;

const BoxWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const BoxSmall = styled.div`
  width: 13rem;
  height: 13rem;
  background-color: #ffff;
  border-radius: 5%;
  margin: 1rem 1rem;
  padding: 1.25rem;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  @media (max-width: 860px) {
    margin: 0.5rem 0.5rem;
    width: 12rem;
    height: 12rem;
  }
`;

const BoxLarge = styled.div`
  width: 31rem;
  height: 13rem;
  background-color: #ffff;
  border-radius: 1rem;
  margin: 1rem 1rem;
  padding: 1.25rem;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  @media (max-width: 860px) {
    margin: 0.5rem 0.5rem;
    width: 30rem;
    height: 12rem;
  }
`;

const BoxTitle = styled.div`
  font-style: normal;
  margin-bottom: 0.5rem;
  font-weight: 500;
  font-size: 1rem;
  line-height: 14px;
  color: #37b4e4;
  @media (max-width: 860px) {
    font-size: 0.75rem;
    margin-bottom: 0.3rem;
  }
`;

const BoxContent = styled.div`
  font-style: normal;
  margin-bottom: 0.5rem;
  font-weight: 500;
  font-size: 1.25rem;
  line-height: 24px;
  color: #68819d;
  @media (max-width: 860px) {
    font-size: 1rem;
  }
`;

const BoxTitleLarge = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 1.5rem;
  line-height: 150%;
  color: #68819d;
  @media (max-width: 860px) {
    font-size: 1.25rem;
  }
`;

const BoxContentLarge = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 1rem;
  line-height: 150%;
  color: #a6b5c6;
  @media (max-width: 860px) {
    font-size: 0.75rem;
  }
`;

const Button = styled.button`
  margin-top: 3.5rem;
  font-style: normal;
  font-weight: 500;
  font-size: 1rem;
  line-height: 150%;
  padding: 0.5rem 1rem;
  color: #ffffff;
  background: #37b4e4;
  border-radius: 0.5rem;
  cursor: pointer;
  user-select: none;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  border: none;
  outline: none;
  @media (max-width: 860px) {
    font-size: 0.75rem;
  }
`;

const ButtonTwo = styled.button`
  margin-top: 1rem;
  font-style: normal;
  font-weight: 500;
  font-size: 1rem;
  line-height: 150%;
  padding: 0.5rem 1rem;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  width: 14rem;
  color: #37b4e4;
  background: #ffff;
  border-radius: 0.5rem;
  cursor: pointer;
  user-select: none;
  border: 1px solid #37b4e4;
  outline: none;
`;
