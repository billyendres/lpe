import React, { useContext, useEffect } from "react";
import { Link } from "gatsby";
import styled from "styled-components";
import { GlobalContext } from "../context/GlobalContext";
import Layout from "../components/Layout";
import backgroundImg from "./images/background2.jpg";

const AverageBill = () => {
  const [
    postCode,
    setPostCode,
    billData,
    setBillData,
    adultsNoBill,
    setAdultsNoBill,
    childrenNoBill,
    setChildrenNoBill,
    adults,
    setAdults,
    child,
    setChild,
  ] = useContext(GlobalContext);

  const averageBill = () => {
    if (childrenNoBill + adultsNoBill === 1) {
      return `The Average Quarterly Bill For A Household With ${adultsNoBill +
        childrenNoBill} Resident is $356.`;
    } else if (childrenNoBill + adultsNoBill === 2) {
      return `The Average Quarterly Bill For A Household With ${adultsNoBill +
        childrenNoBill} Residents is $517.`;
    } else if (childrenNoBill + adultsNoBill === 3) {
      return `The Average Quarterly Bill For A Household With ${adultsNoBill +
        childrenNoBill} Residents is $584.`;
    } else if (childrenNoBill + adultsNoBill === 4) {
      return `The Average Quarterly Bill For A Household With ${adultsNoBill +
        childrenNoBill} Residents is $654.`;
    } else if (childrenNoBill + adultsNoBill > 4) {
      return `The Average Quarterly Bill For A Household With 5+ Residents is $711.`;
    }
  };

  const averageSavings = () => {
    if (childrenNoBill + adultsNoBill === 1) {
      return `You Could Save Approximately $61 By Switching to LPE.`;
    } else if (childrenNoBill + adultsNoBill === 2) {
      return `You Could Save Approximately $97 By Switching to LPE.`;
    } else if (childrenNoBill + adultsNoBill === 3) {
      return `You Could Save Approximately $112 By Switching to LPE.`;
    } else if (childrenNoBill + adultsNoBill === 4) {
      return `You Could Save Approximately $128 By Switching to LPE.`;
    } else if (childrenNoBill + adultsNoBill > 4) {
      return `You Could Save Approximately $141 By Switching to LPE.`;
    }
  };

  return (
    <Layout>
      <Background src={backgroundImg} alt={backgroundImg} />
      <Wrap>
        <TextWrap>
          <Link to="/" style={{ textDecoration: "none" }}>
            <SubHeader>Back to your profile</SubHeader>
          </Link>
          <Header>Your Energy Breakdown</Header>
        </TextWrap>
        <BoxWrap>
          <BoxLarge>
            <BoxTitle>Your bill comparison</BoxTitle>
            <BoxContent>Average Bill For A Home Similar To Yours</BoxContent>
            <AverageBills>{averageBill()}</AverageBills>
            <AverageSavings>{averageSavings()}</AverageSavings>
            <Link to="/">
              <Button>Return To Your Energy Profile</Button>
            </Link>
          </BoxLarge>
        </BoxWrap>
      </Wrap>
    </Layout>
  );
};

export default AverageBill;

const Background = styled.img`
  position: fixed;
  width: 100vw;
  height: 100vh;
  object-fit: cover;
  margin: 0;
`;

const Wrap = styled.div`
  position: absolute;
  z-index: 1;
  top: 10%;
  bottom: 5%;
  left: 5%;
  right: 5%;
`;

const TextWrap = styled.div`
  z-index: 1;
  text-align: center;
  margin: 2rem 0.5rem;
`;

const Header = styled.h1`
  font-style: normal;
  font-weight: bold;
  font-size: 2.5rem;
  line-height: 110%;
  color: #ffff;
  margin: 0;
  text-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  @media (max-width: 860px) {
    font-size: 1.75rem;
  }
`;

const SubHeader = styled.h2`
  font-style: normal;
  font-weight: 600;
  font-size: 1.25rem;
  line-height: 150%;
  text-transform: uppercase;
  color: #ffff;
  margin: 0;
  text-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  @media (max-width: 860px) {
    font-size: 1rem;
  }
`;

const BoxWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const BoxLarge = styled.div`
  width: 35rem;
  height: 21rem;
  background-color: #ffff;
  border-radius: 1rem;
  margin: 1rem 1rem;
  padding: 1.5rem;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  @media (max-width: 860px) {
    margin: 0.5rem 0.5rem;
    width: 33rem;
    height: 17rem;
  }
`;

const BoxTitle = styled.div`
  margin-bottom: 0.5rem;
  font-style: normal;
  font-weight: 500;
  font-size: 1rem;
  line-height: 14px;
  color: #37b4e4;
  margin-bottom: 0.5rem;
`;

const BoxContent = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 1.25rem;
  line-height: 24px;
  color: #68819d;
  width: 20rem;
`;

const AverageBills = styled.div`
  display: flex;
  align-items: center;
  font-style: normal;
  font-weight: 700;
  font-size: 1.5rem;
  line-height: 40px;
  color: #68819d;
  margin-top: 2rem;
  margin-right: 1.5rem;
  @media (max-width: 860px) {
    font-size: 1.25rem;
    line-height: 25px;
  }
`;

const AverageSavings = styled.div`
  display: flex;
  align-items: center;
  font-style: normal;
  font-weight: 700;
  font-size: 1.5rem;
  line-height: 40px;
  color: #37b4e4;
  margin-top: 2rem;
  margin-right: 1.5rem;
  @media (max-width: 860px) {
    font-size: 1.25rem;
    line-height: 25px;
  }
`;

const Button = styled.button`
  margin-top: 2.5rem;
  font-style: normal;
  font-weight: 500;
  font-size: 1rem;
  line-height: 150%;
  padding: 0.75rem 1.5rem;
  color: #ffffff;
  background: #37b4e4;
  border-radius: 0.5rem;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  cursor: pointer;
  user-select: none;
  border: none;
  outline: none;
  @media (max-width: 860px) {
    font-size: 0.75rem;
    margin-top: 2.25rem;
  }
`;
