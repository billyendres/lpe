import React from "react";
import { Link } from "gatsby";
import styled from "styled-components";
import Layout from "../components/Layout";
import PieChart from "../components/PieChart";
import BarGraphBill from "../components/BarGraphBill";
import backgroundImg from "./images/background2.jpg";

const BillBreakdown = () => {
  return (
    <Layout>
      <Background src={backgroundImg} alt={backgroundImg} />
      <Wrap>
        <TextWrap>
          <Link to="/" style={{ textDecoration: "none" }}>
            <SubHeader>Back to your profile</SubHeader>
          </Link>
          <Header>Your Energy Breakdown</Header>
        </TextWrap>
        <BoxWrap>
          <BoxSmall>
            <BoxTitle>1. Your cost breakdown</BoxTitle>
            <BoxContent>Where is your money going?</BoxContent>
            <PieChart />
          </BoxSmall>
          <BoxLarge>
            <BoxTitle>2. Your bill comparison</BoxTitle>
            <BoxContent>
              How does your bill compare to others in the area?
            </BoxContent>
            <BarGraphBill id="bargraph" />
          </BoxLarge>
        </BoxWrap>
      </Wrap>
    </Layout>
  );
};

export default BillBreakdown;

const Background = styled.img`
  position: fixed;
  width: 100vw;
  height: 100vh;
  object-fit: cover;
  margin: 0;
`;

const Wrap = styled.div`
  position: absolute;
  z-index: 1;
  top: 10%;
  bottom: 5%;
  left: 5%;
  right: 5%;
`;

const TextWrap = styled.div`
  z-index: 1;
  text-align: center;
  margin: 2rem 0.5rem;
`;

const Header = styled.h1`
  font-style: normal;
  font-weight: bold;
  font-size: 2.5rem;
  line-height: 110%;
  color: #ffff;
  margin: 0;
  text-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  @media (max-width: 860px) {
    font-size: 1.75rem;
  }
`;

const SubHeader = styled.h2`
  font-style: normal;
  font-weight: 600;
  font-size: 1.25rem;
  line-height: 150%;
  text-transform: uppercase;
  color: #ffff;
  margin: 0;
  text-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  @media (max-width: 860px) {
    font-size: 1rem;
  }
`;

const BoxWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const BoxSmall = styled.div`
  width: 18rem;
  height: 24rem;
  background-color: #ffff;
  border-radius: 5%;
  margin: 1rem 1rem;
  padding: 1.5rem;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  /* @media (max-width: 860px) {
    margin: 0.5rem 0.5rem;
    width: 16rem;
    height: 22rem;
  } */
`;

const BoxLarge = styled.div`
  width: 38rem;
  height: 24rem;
  background-color: #ffff;
  border-radius: 1rem;
  margin: 1rem 1rem;
  padding: 1.5rem;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  @media (max-width: 860px) {
    margin: 0.5rem 0.5rem;
    width: 36rem;
    height: 22rem;
  }
`;

const BoxTitle = styled.div`
  margin-bottom: 0.5rem;
  font-style: normal;
  font-weight: 500;
  font-size: 1rem;
  line-height: 14px;
  color: #37b4e4;
  margin-bottom: 0.5rem;
  /* @media (max-width: 860px) {
    font-size: 0.75rem;
    margin-bottom: 0.3rem;
  } */
`;

const BoxContent = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 1.25rem;
  line-height: 24px;
  color: #68819d;
  width: 20rem;
  /* @media (max-width: 860px) {
    font-size: 1rem;
  } */
`;
