import React, { useState, useEffect, useRef } from "react";
import { Link } from "gatsby";
import styled from "styled-components";
import Layout from "../components/Layout";
import backgroundImg from "./images/background.jpg";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

// const useStateWithLocalStorage = localStorageKey => {
//   const [billingPeriod, setBillingPeriod] = useState(
//     localStorage.getItem(localStorageKey) || ""
//   );
//   useEffect(() => {
//     localStorage.setItem(localStorageKey, billingPeriod);
//   }, [billingPeriod, localStorageKey]);
//   return [billingPeriod, setBillingPeriod];
// };
const useStyles = makeStyles(theme => ({
  formControl: {
    margin: "0.25rem 0",
    minWidth: 150,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const SolarCalculator = () => {
  const [labelWidth, setLabelWidth] = useState(0);
  const [totalBill, setTotalBill] = useState("");
  const [savings, setSavings] = useState(0);
  const [kwPh, setKwph] = useState(0);
  const [solarSize, setSolarSize] = useState(4);

  const classes = useStyles();

  // const [billingPeriod, setBillingPeriod] = useStateWithLocalStorage(
  //   "myValueInLocalStorage"
  // );

  const inputLabel = useRef(null);
  useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  const tarifOriginal = 0.31;
  const tarifLpe = 0.24;

  useEffect(() => {
    if (!totalBill) {
      setKwph(0);
      setSavings(0);
    } else {
      setKwph((totalBill - 90) / tarifOriginal);
      let totalBillLpe = tarifLpe * kwPh + 90;
      setSavings(totalBill - totalBillLpe - 90);
    }
  }, [totalBill, kwPh, savings]);

  const handleSubmit = evt => {
    evt.preventDefault();
  };

  // const onChange = event => {
  //   setBillingPeriod(event.target.value);
  // };

  const solarFeedIn = 0.089;

  const averageSolarGen = 8 * solarSize * 90;

  // const billCycleSavings = averageSolarGen * solarFeedIn * solarSize;

  const annualSavings = () => {
    if (solarSize === 2) {
      return "$520";
    } else if (solarSize === 4) {
      return "$1040";
    } else if (solarSize === 6) {
      return "$1560";
    } else if (solarSize === 8) {
      return "$2080";
    }
  };

  const paybackCost = () => {
    if (solarSize === 2) {
      return "$1200 - $1600";
    } else if (solarSize === 4) {
      return "$1800 - $2500";
    } else if (solarSize === 6) {
      return "$3200 - $4000";
    } else if (solarSize === 8) {
      return "$4500 - $6000";
    }
  };

  const estimatedPayback = () => {
    if (solarSize === 2) {
      return "1 - 2 Years";
    } else if (solarSize === 4) {
      return "2 - 3 Years";
    } else if (solarSize === 6) {
      return "3 - 4 Years";
    } else if (solarSize === 8) {
      return "4 - 5 Years";
    }
  };

  return (
    <div>
      <Layout>
        <Background src={backgroundImg} alt={backgroundImg} />
        <Wrap>
          <TextWrap>
            <SubHeader>
              <Link style={{ textDecoration: "none", color: "white" }} to="/">
                Back to your profile
              </Link>
            </SubHeader>
            <Header>Your Solar Savings</Header>
            <Text>Based on your household size, you could save up to</Text>
            <Savings>{annualSavings()}</Savings>
            <SavingsText>Per Year</SavingsText>
          </TextWrap>
          <BoxSmall>
            <div
              style={{
                borderBottom: "1px solid #D3DFEC",
              }}
            >
              <BoxHeader>Total Cost</BoxHeader>
              <Cost>{paybackCost()}</Cost>
              <BoxHeader style={{ paddingBottom: "1rem" }}>
                <span style={{ marginRight: "1rem" }}>for a</span>
                <FormControl variant="outlined" className={classes.formControl}>
                  <InputLabel
                    ref={inputLabel}
                    id="demo-simple-select-outlined-label"
                  >
                    Please Select
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={solarSize}
                    onChange={e => setSolarSize(e.target.value)}
                    labelWidth={labelWidth}
                  >
                    <MenuItem value={2}>
                      <span style={{ color: "#37B4E4" }}>2kw</span>
                    </MenuItem>
                    <MenuItem value={4}>
                      <span style={{ color: "#37B4E4" }}>4kw</span>
                    </MenuItem>
                    <MenuItem value={6}>
                      <span style={{ color: "#37B4E4" }}>6kw</span>
                    </MenuItem>
                    <MenuItem value={8}>
                      <span style={{ color: "#37B4E4" }}>8kw</span>
                    </MenuItem>
                  </Select>
                </FormControl>
                <span style={{ marginLeft: "1rem" }}>system</span>
              </BoxHeader>
            </div>
            <BoxHeader style={{ paddingTop: "1rem" }}>Est. payback</BoxHeader>
            <Cost>{estimatedPayback()}</Cost>
            <Button>+ Add a battery</Button>
          </BoxSmall>
        </Wrap>
      </Layout>
    </div>
  );
};

export default SolarCalculator;

const Background = styled.img`
  position: fixed;
  width: 100vw;
  height: 100vh;
  object-fit: cover;
  margin: 0;
`;

const Wrap = styled.div`
  position: absolute;
  z-index: 1;
  top: 10%;
  bottom: 5%;
  left: 5%;
  right: 5%;
`;

const TextWrap = styled.div`
  z-index: 1;
  width: 25rem;
`;

const Header = styled.h1`
  font-style: normal;
  font-weight: bold;
  font-size: 2.5rem;
  line-height: 110%;
  color: #ffff;
  margin: 1rem 0;
  text-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  @media (max-width: 860px) {
    font-size: 1.75rem;
  }
`;

const SubHeader = styled.h2`
  font-style: normal;
  font-weight: 600;
  font-size: 1.25rem;
  line-height: 150%;
  text-transform: uppercase;
  color: #ffff;
  text-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  margin: 2rem 0 0;
  @media (max-width: 860px) {
    font-size: 1rem;
  }
`;

const Text = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 1.5rem;
  line-height: 37px;
  text-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  color: #ffffff;
  @media (max-width: 860px) {
    font-size: 1.25rem;
  }
`;

const Savings = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 2.5rem;
  line-height: 70px;
  color: #ffffff;
  text-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  padding-right: 1.5rem;
  @media (max-width: 860px) {
    font-size: 1.75rem;
  }
`;

const SavingsText = styled.span`
  font-style: normal;
  font-weight: 600;
  font-size: 1.5rem;
  line-height: 45px;
  text-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  color: #ffffff;
  @media (max-width: 860px) {
    font-size: 1.25rem;
  }
`;

const BoxSmall = styled.div`
  width: 18rem;
  height: 18rem;
  background-color: #ffff;
  border-radius: 5%;
  margin: 1rem 0;
  padding: 1.25rem;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  @media (max-width: 860px) {
    margin: 0.5rem 0;
    width: 12rem;
    height: 12rem;
  }
`;

const BoxHeader = styled.div`
  display: flex;
  align-items: center;
  font-style: normal;
  font-weight: 500;
  font-size: 1.25rem;
  line-height: 24px;
  color: #68819d;
  margin-top: 0.5rem;
  @media (max-width: 860px) {
    font-size: 1rem;
  }
`;

const Cost = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 1.5rem;
  line-height: 150%;
  color: #68819d;
  @media (max-width: 860px) {
    font-size: 1.25rem;
  }
`;

const Button = styled.button`
  margin-top: 2.5rem;
  font-style: normal;
  font-weight: 500;
  font-size: 1rem;
  line-height: 150%;
  padding: 0.75rem 1.5rem;
  color: #ffffff;
  background: #37b4e4;
  border-radius: 0.5rem;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  cursor: pointer;
  user-select: none;
  border: none;
  outline: none;
  @media (max-width: 860px) {
    font-size: 0.75rem;
    margin-top: 2.25rem;
  }
`;
