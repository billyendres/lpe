import React from "react";
import ProfileBuilder from "./profile";
import Layout from "../components/Layout";

const App = () => {
  return (
    <div>
      <Layout>
        <ProfileBuilder />
      </Layout>
    </div>
  );
};

export default App;
