import React from "react";
import { Link } from "gatsby";
import styled from "styled-components";
import BurgerMenu from "./BurgerMenu";
import CollapseMenu from "./CollapseMenu";
import logo from "../../pages/images/logo.svg";

const Navbar = ({ navbarState, handleNavbar }) => {
  return (
    <div style={{ position: "fixed", zIndex: "10" }}>
      <NavBar>
        <FlexContainer>
          <Link to="/">
            <Logo src={logo} alt={logo} />
          </Link>
          <NavLinks>
            <Link to="/">Profile Builder</Link>
            <Link to="/breakdown">Bill Breakdown</Link>
            <Link to="/solar">Solar Calculator</Link>
          </NavLinks>
          <BurgerWrapper>
            <BurgerMenu navbarState={navbarState} handleNavbar={handleNavbar} />
          </BurgerWrapper>
        </FlexContainer>
      </NavBar>
      <CollapseMenu navbarState={navbarState} handleNavbar={handleNavbar} />
    </div>
  );
};

export default Navbar;

const NavBar = styled.nav`
  width: 100vw;
  top: 0;
  left: 0;
  font-size: 1.4rem;
`;

const FlexContainer = styled.div`
  display: flex;
  align-items: center;
  padding: 0 4.5rem;
  justify-content: space-between;
  height: 4rem;
  background: linear-gradient(
    180deg,
    rgba(0, 0, 0, 0.1875) 0%,
    rgba(0, 0, 0, 0) 100%
  );
`;

const NavLinks = styled.ul`
  & a {
    text-transform: uppercase;
    font-style: normal;
    font-weight: 500;
    font-size: 1rem;
    line-height: 150%;
    color: #ffffff;
    border-bottom: 1px solid transparent;
    margin: 0 1.5rem;
    transition: all 300ms linear 0s;
    text-decoration: none;
    cursor: pointer;

    &:hover {
      color: #37b4e4;
      border-bottom: 1px solid #37b4e4;
    }

    @media (max-width: 860px) {
      display: none;
    }
  }
`;

const BurgerWrapper = styled.div`
  margin: auto 0;

  @media (min-width: 860px) {
    display: none;
  }
`;

const Logo = styled.img`
  width: 5rem;
`;
