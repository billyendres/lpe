import React from "react";
import styled from "styled-components";
import { Link } from "gatsby";
import { useSpring, animated } from "react-spring";

const CollapseMenu = props => {
  const { open } = useSpring({ open: props.navbarState ? 0 : 1 });

  if (props.navbarState === true) {
    return (
      <CollapseWrapper
        style={{
          transform: open
            .interpolate({
              range: [0, 0.2, 0.3, 1],
              output: [0, -20, 0, -200],
            })
            .interpolate(openValue => `translate3d(0, ${openValue}px, 0`),
        }}
      >
        <NavLinks>
          <li>
            <a>
              <Link to="/">Profile Builder</Link>
            </a>
          </li>
          <li>
            <a>
              <Link to="/breakdown">Bill Breakdown</Link>
            </a>
          </li>
          <li>
            <a>
              <Link to="/solar">Solar Calculator</Link>
            </a>
          </li>
        </NavLinks>
      </CollapseWrapper>
    );
  }
  return null;
};

export default CollapseMenu;

const CollapseWrapper = styled(animated.div)`
  background: #2d3436;
  padding: 0.5rem 0;
`;

const NavLinks = styled.ul`
  list-style-type: none;
  padding-right: 4rem;
  text-align: right;
  user-select: none;

  & li {
    transition: all 300ms linear 0s;
  }

  & a {
    font-size: 1rem;
    line-height: 2;
    color: #dfe6e9;
    text-transform: uppercase;
    text-decoration: none;
    cursor: pointer;
    border-bottom: 1px solid transparent;
    transition: all 300ms linear 0s;

    &:hover {
      color: #37b4e4;
      border-bottom: 1px solid #37b4e4;
    }
  }
`;
