import React, { useState } from "react";
import * as d3 from "d3";
import styled from "styled-components";

const PieChart = () => {
  const width = 285;
  const height = 220;
  const radius = 100;
  const [data, setData] = useState([
    {
      category: "Generation",
      percentage: 35,
      color: "#37B4E4",
    },
    {
      category: "Metering",
      percentage: 4,
      color: "#A2DFF7",
    },
    {
      category: "Network",
      percentage: 39,
      color: "#68819D",
    },
    {
      category: "Retail",
      percentage: 22,
      color: "#A6B5C6",
    },
  ]);

  const pie = d3.pie().value(d => d.percentage)(data);
  const arc = d3
    .arc()
    .outerRadius(radius - 10)
    .innerRadius(40);

  return (
    <>
      <svg height={height - 15}>
        <g transform={`translate(${width / 2},${height / 2})`}>
          {pie.map((d, i) => {
            return (
              <g key={i}>
                <path key={i} d={arc(d)} fill={d.data.color} />
              </g>
            );
          })}
        </g>
      </svg>
      <div>
        <Text
          style={{ color: "#37B4E4" }}
        >{`${data[0].percentage}% goes to ${data[0].category}`}</Text>
        <Text
          style={{ color: "#A2DFF7" }}
        >{`${data[1].percentage}% goes to ${data[1].category}`}</Text>
        <Text
          style={{ color: "#68819D" }}
        >{`${data[2].percentage}% goes to ${data[2].category}`}</Text>
        <Text
          style={{ color: "#A6B5C6" }}
        >{`${data[3].percentage}% goes to ${data[3].category}`}</Text>
      </div>
    </>
  );
};

export default PieChart;

const Text = styled.div`
  font-family: nudista-web, sans-serif;
  font-size: 1rem;
  padding: 0.5rem;
  text-align: center;
`;
