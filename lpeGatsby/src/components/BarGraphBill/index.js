import React, { useEffect, useContext } from "react";
import { GlobalContext } from "../../context/GlobalContext";
import styled from "styled-components";
import { Link } from "gatsby";
import * as d3 from "d3";

const BarGraphBill = ({ id }) => {
  const [
    postCode,
    setPostCode,
    billData,
    setBillData,
    child,
    setChild,
    adults,
    setAdults,
  ] = useContext(GlobalContext);

  useEffect(() => {
    const svg = d3
      .select("#" + id)
      .append("svg")
      .attr("class", "background-style")
      .attr("width", 300)
      .attr("height", 226)
      .style("margin-left", -25);
    const margin = { top: 170, right: 0, bottom: 0, left: 0 };
    const width = 300;
    const height = 80;

    const x = d3
      .scaleBand()
      .rangeRound([70, width])
      .padding(0.2);
    const y = d3.scaleLinear().rangeRound([height, -150]);

    const g = svg
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    x.domain(
      billData.map(d => {
        return d.name;
      })
    );
    y.domain([
      0,
      d3.max(billData, d => {
        return d.value;
      }),
    ]);
    const draw = billData => {
      g.selectAll(".bar")
        .data(billData)
        .enter()
        .append("rect")
        .attr("class", "bar")
        .attr("fill", (d, i) => d.color)
        .attr("x", d => {
          return x(d.name);
        })
        .attr("y", d => {
          return y(d.value);
        })
        .attr("width", x.bandwidth())
        .attr("height", d => {
          return height - y(d.value);
        });
    };
    draw(billData);
  }, [billData, id]);

  return (
    <Wrap>
      <GraphWrap>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            width: "11rem",
            marginLeft: "4.5rem",
          }}
        >
          <YourBillText>{`$${billData[0].value}`}</YourBillText>
          <AverageBillText>{`$${billData[1].value}`}</AverageBillText>
          <SimilarBillText>{`$${billData[2].value}`}</SimilarBillText>
        </div>
        <div id={id} />
        <GraphTextWrap>
          <GraphText>
            <div
              style={{
                width: "3rem",
                display: "inline-block",
                margin: "0 1rem",
              }}
            >
              Your Bill
            </div>
            <div
              style={{
                width: "4rem",
                display: "inline-block",
              }}
            >
              Location's Average Bill
            </div>
            <div
              style={{
                width: "5rem",
                display: "inline-block",
              }}
            >
              Bills of similarly sized homes
            </div>
          </GraphText>
        </GraphTextWrap>
      </GraphWrap>
      <SavingsWrap>
        <Savings>
          {billData[0].value - billData[2].value > 0 ? (
            <>
              You could save on average
              <span style={{ color: "#37b4e4", marginLeft: "0.5rem" }}>
                {`$${billData[0].value - billData[2].value} `}
              </span>
              every quarter by switching to LPE.
            </>
          ) : (
            <>Your bill is less than others in your area.</>
          )}
        </Savings>
        <ButtonOne>See How You Can Save</ButtonOne>
        <Link to="/solar">
          <ButtonTwo>See Your Solar Savings</ButtonTwo>
        </Link>
        <Link to="/">
          <ButtonThree>Change Your Details</ButtonThree>
        </Link>
      </SavingsWrap>
    </Wrap>
  );
};

export default BarGraphBill;

const Wrap = styled.div`
  height: 17rem;
  display: flex;
  justify-content: space-between;
`;

const GraphWrap = styled.div`
  background: #f7fbfc;
  margin-top: 1.5rem;
  height: 15.6rem;
  flex: 1;
  border-radius: 5%;
`;

const GraphTextWrap = styled.div`
  width: 20rem;
  margin: 0.5rem 0;
`;

const GraphText = styled.div`
  font-family: nudista web "sans-serif";
  font-weight: 500;
  font-size: 0.7rem;
  line-height: 14px;
  color: #68819d;
  text-align: center;
  display: flex;
  align-items: top;
  justify-content: center;
`;

const YourBillText = styled.div`
  display: inline-block;
  font-style: normal;
  font-weight: 600;
  font-size: 1rem;
  line-height: 150%;
  color: #37b4e4;
`;

const AverageBillText = styled.div`
  display: inline-block;
  font-style: normal;
  font-weight: 600;
  font-size: 1rem;
  line-height: 150%;
  color: #a2dff7;
`;

const SimilarBillText = styled.div`
  display: inline-block;
  font-style: normal;
  font-weight: 600;
  font-size: 1rem;
  line-height: 150%;
  color: #68819d;
`;

const SavingsWrap = styled.span`
  width: 16rem;
  margin-top: 1rem;
  padding-left: 1.5rem;
`;

const Savings = styled.span`
  font-family: nudista-web, sans-serif;
  font-style: normal;
  font-weight: 400;
  font-size: 1.4rem;
  color: #68819d;
`;

const ButtonOne = styled.button`
  margin-top: 1rem;
  font-style: normal;
  font-weight: 500;
  font-size: 1rem;
  line-height: 150%;
  padding: 0.5rem 1rem;
  width: 16rem;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  color: #ffff;
  background: #37b4e4;
  border-radius: 0.5rem;
  cursor: pointer;
  user-select: none;
  border: none;
  outline: none;
`;

const ButtonTwo = styled.button`
  margin-top: 1rem;
  font-style: normal;
  font-weight: 500;
  font-size: 1rem;
  line-height: 150%;
  padding: 0.5rem 1rem;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  width: 14rem;
  color: #37b4e4;
  background: #ffff;
  border-radius: 0.5rem;
  cursor: pointer;
  user-select: none;
  border: 1px solid #37b4e4;
  outline: none;
`;

const ButtonThree = styled.button`
  margin-top: 1rem;
  font-style: normal;
  font-weight: 500;
  font-size: 1rem;
  line-height: 150%;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
  padding: 0.5rem 1rem;
  width: 12rem;
  color: #37b4e4;
  background: #ffff;
  border-radius: 0.5rem;
  cursor: pointer;
  user-select: none;
  border: 1px solid #37b4e4;
  outline: none;
`;
