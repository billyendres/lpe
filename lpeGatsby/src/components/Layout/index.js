import React, { useState } from "react";
import Navbar from "../Header/index.js";
import { createGlobalStyle } from "styled-components";

const Layout = ({ children }) => {
  const [navbar, setNavbar] = useState(false);

  const handleNavbar = () => {
    setNavbar(!navbar);
  };
  return (
    <div>
      <Navbar navbarState={navbar} handleNavbar={handleNavbar} />
      <GlobalStyle />
      <div>{children}</div>
    </div>
  );
};

export default Layout;

const GlobalStyle = createGlobalStyle`
  body {
    font-family: nudista-web, sans-serif;
		margin: 0;
    padding: 0;
    font-size: 1rem;
    display: flex;
    align-content: center;
    align-items: center;
  }
`;
